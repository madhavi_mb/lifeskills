### OSI Model

For standardized communication between computers, ISO introduced the OSI model.

It has seven layers. They are (in a top-down format):

1. **Application Layer**

   It is the topmost layer. It provides protocols to be used by the applications. FTP is used for file transfer, HTTP or HTTPS is used for web surfing, SMTP is used for emails, Telnet is used for virtual terminals.

2. **Presentation Layer**

   This layer is also called the translation layer as it converts the data received from the application layer into binary format. This helps in data compression and easier transmission.

3. **Session Layer**

   This layer helps to establish a session, maintain it, and also terminate it once the task gets finished. It ensures security.

4. **Transport Layer**

   It takes the message from the session layer, breaks it into smaller units. These units, after being sent to the destination, are reassembled by the transport layer there. It helps maintain the rate at which the data is being sent and received. Checksum is used for data integrity. TCP, UDP are used; In TCP, if packets are lost, data is retransmitted, whereas in UDP, no such feedback is provided.

5. **Network Layer**

   This layer is responsible for sending data between hosts present in different networks. It also helps in efficient packet routing. The sender & receiver’s IP addresses are placed in the header by the network layer.

6. **Data Link Layer**

   The data link layer helps in node-to-node delivery of the message. It ensures that data transfer takes place without errors.

7. **Physical Layer**

   This is the lowest layer, and it is responsible for the actual physical connection between devices.

---

Reference: [GeeksforGeeks - Open Systems Interconnection Model (OSI)](https://www.geeksforgeeks.org/open-systems-interconnection-model-osi/)
