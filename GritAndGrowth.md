### Grit and Growth Mindset

1. In the video, the speaker emphasizes that continued effort and passion over time are crucial for achieving meaningful accomplishments. She encourages people to embrace challenges, stay dedicated to goals, and highlights that these factors can be more decisive in success than talent alone.

2. In a fixed mindset, individuals believe they have a set amount of intelligence or talent, while a growth mindset is based on the belief that abilities can be developed through hard work and dedication. With a growth mindset, failures are seen not as indicators of personal failure or intelligence but rather as opportunities for improvement.

3. Having an internal locus of control means believing that you have control over your life and outcomes through your own efforts. In this mindset, individuals take responsibility, set goals, and strive to fulfill them.

4. Key points mentioned include believing that you can figure things out, questioning assumptions that limit your growth, and maintaining resilience.

5. To build a growth mindset, one should try new things, work hard to achieve them, listen to feedback, and not give up when faced with challenges.

Reference : https://hbr.org/2016/01/what-having-a-growth-mindset-actually-means