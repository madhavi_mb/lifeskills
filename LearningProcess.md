### Learning Process

#### Feynman Technique

The Feynman technique is a learning strategy that involves teaching a concept in simple terms to improve understanding and retention.

#### Focus and Diffuse Modes

- **Focus Mode:** Like a spotlight, concentrating on a specific task.
- **Diffuse Mode:** Like a background process, allowing your mind to make connections and solve problems in a more relaxed state.

#### Active and Diffused Modes of Thinking

- **Active Mode:** Involves focusing on a specific task.
- **Diffused Mode:** Letting your mind wander and make connections in a more relaxed state.

#### Learning Process Steps

1. **Preview:**
   - Go through the material to get an overview.

2. **Set Objectives:**
   - Clearly define what you want to achieve with this learning session. Set specific goals or questions to answer.

3. **Active Reading/Listening:**
   - Engage actively with the material. Take notes, highlight key points, and ask questions. If it's a lecture or video, actively listen and take brief notes.

4. **Break Down Into Parts:**
   - Divide the topic into smaller, manageable sections. Tackle one part at a time to avoid feeling overwhelmed.

5. **Concept Mapping:**
   - Create a simple concept map or outline to visualize the relationships between different parts of the topic.

6. **Deep Dive:**
   - Go deeper into each subtopic. Read, watch, or listen more attentively. Take detailed notes and try to understand the underlying principles.

7. **Connect to Prior Knowledge:**
   - Relate new information to what you already know. This helps in better retention and understanding.

8. **Practice:**
   - Apply what you've learned through practice problems, discussions, or practical exercises. This reinforces your understanding.

9. **Teach/Explain:**
   - Teaching someone else is a powerful way to solidify your understanding. Explain the topic in your own words.

10. **Reflect and Review:**
    - Reflect on what you've learned. Review your notes and see if you can recall key points without looking. Identify areas where you might need more clarification.

11. **Seek Clarification:**
    - If you encounter difficulties, don't hesitate to seek clarification from teachers, peers, or additional resources.

12. **Create Summaries:**
    - Write a summary of the topic in your own words. This reinforces your learning and provides a quick reference for later.

13. **Regular Review:**
    - Periodically revisit the topic to reinforce your memory. Spaced repetition is a powerful technique for long-term retention.

14. **Apply in Real Life:**
    - Whenever possible, try to apply what you've learned in real-life situations. This helps in connecting theoretical knowledge with practical scenarios.

15. **Stay Curious:**
    - Maintain a curious mindset. Ask questions and explore related topics. Learning is a continuous process.

I would like to follow the above mentioned points.

Reference : https://www.routledge.com/blog/article/the-5-steps-of-the-learning-cycle
