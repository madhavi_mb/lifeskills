### Tiny habits

Applying B = MAP: Behavior equals Motivation, Ability, and Prompt to make new habits easier

**Boost Motivation (M):**
- Understand why the habit matters to you.
- Picture the good things that will come from it.
- Set goals that mean something to you.

**Enhance Ability (A):**
- Keep the habit simple and doable.
- Break it into smaller steps.
- Remove anything that makes it hard.

**Use Prompts (P):**
- Create reminders or triggers for the habit.
- Connect it to things you already do.
- Set alarms or cues to help you remember.

By thinking about Motivation, Ability, and Prompts, you can make forming new habits easier and more likely to stick.

Celebrating after completing a habit is like appreciating yourself for doing well. It makes you happy and motivates you to maintain the habit.

1% Better Every Day

1. **Small Consistent Efforts:** Focus on making tiny improvements each day, whether it's learning a new skill, exercising, or developing a habit. These small changes, when done consistently, add up over time.

2. **Continuous Learning:** Embrace a mindset of continuous improvement. Whether gaining knowledge, refining a skill, or enhancing a habit, the goal is to keep progressing in small increments every day.

3. **Long-Term Growth:** The 1% improvement daily approach emphasizes long-term growth and success. By consistently making small positive changes, you can achieve significant transformations and reach your goals over time.

Atomic Habits Book Insights

1. **Identity Perspective:** The book suggests viewing habits as part of your identity, believing that you are already the person who has those habits. This makes it easier to align your actions with that identity.

2. **Making Habits Easier:** Pair a new habit with an existing one, so the established behavior reminds you and makes it simpler to integrate the new habit into your routine.

3. **Making Habits Harder:** Introduce obstacles or reduce convenience for unwanted habits, creating barriers that make it more difficult to engage in them.

I would like to plan before coding, allotting time and finishing tasks in that manner. I would not like to start the project without planning. To make this habit attractive, when the project is divided into modules, thinking of solving smaller tasks in a shorter duration could be rewarding and easier to approach.

Reference : https://www.freecodecamp.org/news/how-to-be-more-consistent-when-learning-to-code/