#### Sexual Harassment Awareness

Sexual harassment includes a range of unwelcome sexual behaviors that create a hostile or intimidating environment. This can be in the form of comments, gestures, advances, or requests for sexual favors. Addressing such behavior is important for creating a safe and respectful space. 

**Speak Up:**
Clearly and assertively communicate that the behavior is unwelcome and inappropriate.

**Note down the details of the Incident:**
Maintain a detailed record, noting dates, times, locations, and individuals involved.

**Report to Authorities:**
In workplace, promptly report incidents to HR or a supervisor, following the organization's procedures.

**Seek Support:**
Reach out to friends, family, or colleagues for emotional support during challenging times.

**Educate Yourself:**
Familiarize yourself with your rights and the policies in place to address sexual harassment in your community or workplace.

**Legal Action:**
If necessary, consult with legal professionals to explore potential legal avenues and protections.

**Promote Awareness:**
Encourage open discussions about consent, respect, and appropriate behavior to contribute to a culture of awareness and mutual respect.

Every situation is unique, and responses may vary. Prioritize your well-being, seek assistance, and contribute to creating an environment free from sexual harassment.

Reference : https://hbr.org/2020/05/why-sexual-harassment-programs-backfire
